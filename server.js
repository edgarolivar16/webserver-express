const express = require('express')
const app = express()
const hbs = require('hbs');
require('./hbs/helpers/helpers')
const puerto = process.env.PORT || 3000

/**
 * la funcion res.send detecta el tipo de dato a enviar
 * 
 * express envia el header X-Powered-By : 'Express' con esto algun servicio puede detectar el server tipo walpalayzer
 */

/**
 * un middleware es un callback que se ejecuta previamente, en las rutas que especifique el server
 */

/**
 * considerar que para llamar a home.html es => localhost:3000/home.html
 */

app.use(express.static(__dirname + '/public'))
hbs.registerPartials(__dirname + '/views/parciales');
app.set('view engine', 'hbs');


app.get('/', (req, res) => {
  /**
   * hbs busca si existe una variable en los render, si no existe pasa a los helper
   */
  res.render('home', {
    nombre: 'edGar oliVar',
  })
})

app.get('/about', (req, res) => {
  res.render('about')
})

// app.get('/test',(req, res)=>{
//   let respuesta = {
//     url:  req.url.toString(),
//     nombre: 'Moises'
//   }
//   res.send(respuesta)
// })

// // podemos dejar esta como default
//  app.get('/*',(req, res)=>{
//    let respuesta = {
//      url:  req.url.toString(),
//      nombre: 'No encontrado'
//    }
//    res.send(respuesta)
//  })


app.listen(puerto, () => console.log(`Server on port ${puerto}`))